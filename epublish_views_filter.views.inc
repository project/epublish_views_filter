<?php

/**
 * Implementation of hook_views_data_alter()
 */

function epublish_views_filter_views_data_alter(&$cache) {
  $cache['node']['epublish_publication'] = array(
    'group' => t('E-Publish'),
    'title' => t('Publication'),
    'help' => t('Select nodes from a given publication'),
    'filter' => array(
      'handler' => 'epublish_views_publication_filter_handler',
    ),
  );

  $cache['node']['epublish_edition'] = array(
    'group' => t('E-Publish'),
    'title' => t('Edition'),
    'help' => t('Select nodes from a given edition'),
    'filter' => array(
      'handler' => 'epublish_views_edition_filter_handler',
    ),
  );
}

/**
 * Implementation of hook_views_handlers
 */
function epublish_views_filter_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'epublish_views_filter'),
    ),
    'handlers' => array(
      'epublish_views_publication_filter_handler' => array(
        'parent' => 'views_handler_filter_in_operator',
      ),
      'epublish_views_edition_filter_handler' => array(
        'parent' => 'views_handler_filter_in_operator',
      ),
    ),
  );
}


