<?php
class epublish_views_edition_filter_handler extends views_handler_filter_in_operator {
  function operator_options() {
    return array(
      'in' => t('Is one of (exact)'),
      'tr' => t('Is one of (incl. translations)'),
      'not in' => t('Is not one of'),
    );
  }
  
  function get_value_options() {
    $this->value_options = array();
    
    $order = variable_get('epublish_edition_order', 'DESC');
    $publications = db_query("SELECT * FROM {epublish_publication}");
    while ($publication = db_fetch_object($publications)) {
      $editions = db_query("SELECT * FROM {epublish_edition} WHERE pid=%d", $publication->pid);
      while($edition = db_fetch_object($editions)) {
        $this->value_options[$edition->eid] = t($publication->name).': '.theme('epublish_edition_reference', $edition);
      }
    }
  }

  function query() {
    if (!count($this->value)) {
      return;
    }
    
    $operator = $this->operator;
    $node_alias = 'node';
    if ($operator == 'tr') {
      $operator = 'in';
      $join = new views_join();
      $join->construct('node', 'node', 'tnid', 'tnid');
      $node_alias = $this->query->queue_table('node', null, $join);
      $this->query->add_where($this->options['group'],
        "(node.tnid > 0 OR (node.nid = $node_alias.nid))"
      );
    }
    
    $join1 = new views_join();
    $join1->construct('epublish_edition_node', $node_alias, 'nid', 'nid');
    
    $alias = $this->query->queue_table('epublish_edition_node', null, $join1);
    
    $this->query->add_where($this->options['group'], 
      $alias.'.eid '.$operator.' ('.implode(',', $this->value).')'
    );
  }
}

